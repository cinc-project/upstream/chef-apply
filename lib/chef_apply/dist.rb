module ChefApply
  module Dist
    # This class is not fully implemented, depending on it is not recommended!
    # When referencing a product directly, like Chef (Now Chef Infra)
    PRODUCT = "Cinc Client".freeze

    # The product name as used in mixlib-install for Chef Infra Client
    PRODUCT_NAME = "cinc".freeze

    # The name of the server product
    SERVER_PRODUCT = "Cinc Server".freeze

    # Short name for Chef Infra
    SHORT = "Cinc".freeze

    # The client's alias (chef-client)
    CLIENT = "cinc-client".freeze

    # name of the automate product
    AUTOMATE = "Cinc Dashboard".freeze

    # The chef executable, as in `chef gem install` or `chef generate cookbook`
    EXEC = "cinc".freeze

    # The workstation's product name
    WORKSTATION = "Cinc Workstation".freeze

    # product website address
    WEBSITE = "https://cinc.sh".freeze

    # chef-apply's product name
    APPLY = "cinc-run".freeze

    # chef-apply's executable
    APPLYEXEC = "cinc-apply".freeze

    # chef-run's product name
    RUN = "Cinc Run".freeze

    # chef-run executable
    RUNEXEC = "cinc-run".freeze

    # Chef-Zero's product name
    ZERO = "Cinc Zero".freeze

    # Chef-Solo's product name
    SOLO = "Cinc Solo".freeze

    # The chef-zero executable (local mode)
    ZEROEXEC = "cinc-zero".freeze

    # The chef-solo executable (legacy local mode)
    SOLOEXEC = "cinc-solo".freeze

    # The chef-shell executable
    SHELL = "cinc-shell".freeze

    # Configuration related constants
    # The chef-shell configuration file
    SHELL_CONF = "chef_shell.rb".freeze

    # The configuration directory
    CONF_DIR = "/etc/#{ChefApply::Dist::EXEC}".freeze

    # The user's configuration directory
    USER_CONF_DIR = ".cinc".freeze

    # Workstation user configs
    WORKSTATION_USER_CONF_DIR = ".cinc-workstation".freeze

    # The old ChefDk's product name
    DK = "CincDK".freeze

    # The server's configuration directory
    SERVER_CONF_DIR = "/etc/cinc-server".freeze

    # download.chef.io
    DOWNLOADS_URL = "downloads.cinc.sh".freeze

    # the "chef-workstation" in downloads.chef.io/chef-workstation/stable
    WORKSTATION_URL_SUFFIX = "cinc-workstation".freeze
  end
end
